# Introduction

Image with `steamcmd` and dependencies for building **Source** / **GoldSrc** dedicated server images.

## Supported Tags

* `git`, `latest` [(/git/Dockerfile)](https://gitlab.com/theohbrothers/docker-steamcmd/blob/master/git/Dockerfile)
* `minimal` [(/minimal/Dockerfile)](https://gitlab.com/theohbrothers/docker-steamcmd/blob/master/minimal/Dockerfile)

[![pipeline status](https://gitlab.com/theohbrothers/docker-steamcmd/badges/master/pipeline.svg)](https://gitlab.com/theohbrothers/docker-steamcmd/commits/master)